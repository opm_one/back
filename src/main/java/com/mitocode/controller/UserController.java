package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Menu;
import com.mitocode.model.Usuario;
import com.mitocode.service.ISignoVitalService;
import com.mitocode.service.IUserService;

@RestController
@RequestMapping("/usuarios")
public class UserController {
	
	@Autowired
	private IUserService service;

	@Resource(name = "tokenServices")
	private ConsumerTokenServices tokenServices;

	@GetMapping(value = "/anular/{tokenId:.*}")
	public void revokeToken(@PathVariable("tokenId") String token) {
		tokenServices.revokeToken(token);				
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public  ResponseEntity<Usuario> listarId(@PathVariable("id") Integer id) {
		Usuario user = new Usuario();
		user = service.listarId(id);
		if (user == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}

		return new ResponseEntity<Usuario>(user, HttpStatus.OK);

	}
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Usuario>> listarPageable(Pageable pageable) {
		Page<Usuario> listUser = null;

		listUser = service.listarPageable(pageable);

		return new ResponseEntity<Page<Usuario>>(listUser, HttpStatus.OK);
	}
	
	@PostMapping(value="/userInRol", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrarUserInRol(@Valid @RequestBody Usuario user) {
		 service.registrarUserInRol(user);
			return new ResponseEntity<Object>(HttpStatus.CREATED);

	}
	
	
}
