package com.mitocode.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Integer> {
	
	Usuario findOneByUsername(String username);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO usuario_rol(id_usuario, id_rol) VALUES (:idUsuario, :idRol)", nativeQuery = true)
	int registrarRolInUser(@Param("idUsuario") Integer idUsuario,@Param("idRol") Integer idRol);
}