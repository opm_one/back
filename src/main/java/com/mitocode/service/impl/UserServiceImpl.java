package com.mitocode.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Menu;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUserService;

@Service("userDetailsService")
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUsuarioDAO userDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario user = userDAO.findOneByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("Usuario no existe", username));
		}

		List<GrantedAuthority> authorities = new ArrayList<>();
		user.getRoles().forEach(role -> {
			authorities.add(new SimpleGrantedAuthority(role.getNombre()));
		});

		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),
				user.getPassword(), authorities);

		return userDetails;
	}

	@Override
	public void registrarUserInRol(Usuario usuario) {
		usuario.getRoles().forEach(x -> userDAO.registrarRolInUser(usuario.getIdUsuario(), x.getIdRol()));
		;
	}
	
	@Override
	public Usuario listarId(int idUSer) {
		return userDAO.findOne(idUSer);
	}
	
	@Override
	public Page<Usuario> listarPageable(Pageable pageable) {		
		return userDAO.findAll(pageable);
	}
}