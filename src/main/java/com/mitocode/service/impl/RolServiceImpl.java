package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IMenuDAO;
import com.mitocode.dao.IRolDAO;
import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;
@Service
public class RolServiceImpl implements IRolService {

	@Autowired
	private IRolDAO dao;
	
	@Override
	public Rol registrar(Rol t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public Rol modificar(Rol t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Rol listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Rol> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
