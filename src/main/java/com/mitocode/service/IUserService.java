package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.mitocode.model.Menu;
import com.mitocode.model.Usuario;

public interface IUserService extends UserDetailsService {
	
	void registrarUserInRol(Usuario usuario);
	 Page<Usuario> listarPageable(Pageable pageable) ;
	 Usuario listarId(int idUSer) ;
}
